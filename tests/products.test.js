const dotenv = require('dotenv').config();
const APP_PORT = process.env.APP_PORT;
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);
const API_URL = `http://localhost:${APP_PORT}`;
const id = 2;

describe('PRODUCTS', () => {

  it('return a products list', done => {
    chai.request(API_URL)
    .get('/api/products?field=id,name')
    .end( (err, resp ) => {
      expect(resp).to.have.status(200);
      expect(resp).to.be.an('object');
      expect(resp.body.products[0]).to.have.property('id');
      expect(resp.body.products[0]).to.have.property('name');
      done();
    })
  });

  it('return one product', done => {
    chai.request(API_URL)
    .get('/api/products/' + id)
    .end( (err, resp ) => {
      expect(resp).to.have.status(200);
      expect(resp).to.be.an('object');
      expect(resp.body.product).to.have.property('id');
      expect(resp.body.product).to.have.property('name');
      done();
    })
  })

});