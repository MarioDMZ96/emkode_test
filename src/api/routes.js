const express = require('express');
const app = express();

const productsRoutes = require('./components/products/products.routes');

app.use( '/', productsRoutes );

module.exports = app;