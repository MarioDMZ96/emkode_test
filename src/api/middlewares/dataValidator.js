const Joi = require('joi');

const validateProductCreate = async (req, res, next) => {
    const schema = Joi.object({
        name: Joi.string().max(255).required(),
        brand: Joi.string().max(255).required()
    });
    await validateRequest(schema, req.body, res, next);
}

const validateProductUpdate = async (req, res, next) => {
    const schema = Joi.object({
        name: Joi.string().max(255),
        brand: Joi.string().max(255)
    });
    await validateRequest(schema, req.body, res, next);
}

const validateRequest = async (schema, reqData, res, next) => {
    try {
        await schema.validateAsync(reqData);
        next();
    } catch (errors) {
        res.status(400).json({message: 'Invalid request', errors});
    }
}

module.exports = { 
    validateProductCreate,
    validateProductUpdate
};