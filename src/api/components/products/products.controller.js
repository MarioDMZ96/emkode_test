const { PrismaClient } = require('@prisma/client');
const { createQuery } = require('./products.service');
class Products {

    prisma;

    constructor () {
        this.prisma = new PrismaClient();
    }

    async get (params) {
        try {
            let query = createQuery(params);
            return await this.prisma.products.findMany(query);
        } catch (error) {
            throw error;
        }
    }

    async create (product) {
        try {
            return await this.prisma.products.create({
                data: product,
            });
        } catch (error) {
            throw error;
        }
    }

    async getOne (id) {
        try {
            return await this.prisma.products.findUnique({
                where: {
                    id: Number(id),
                },
            });
        } catch (error) {
            throw error;
        }
    }

    async updateOne (id, product) {
        try {
            return await this.prisma.products.update({
                where: {
                    id: Number(id),
                },
                data: product,
            })
        } catch (error) {
            throw error;
        }
    }

    async deleteOne (id) {
        try {
            return await this.prisma.products.delete({
                where: {
                    id: Number(id),
                },
            })
        } catch (error) {
            throw error;
        }
    }

}

module.exports = Products;