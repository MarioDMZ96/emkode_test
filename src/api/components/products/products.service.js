const createQuery = (params) => {
    let query = { where: {}, select: {}, orderBy: [] };
    let order = { '-': 'desc', '+': 'asc'};
    let field = params.field ? params.field.split(',') : [];
    let sort = params.sort ? params.sort.split(',') : [];
    field.map(item => query.select[item] = true);
    query.where = params.id ? { id: Number(params.id) } : {};
    sort.map(item => {
        let orderBy = item[item.length - 1] !== ' ' ? item[item.length - 1] : '+';
        let field = item.slice(0,-1);
        query.orderBy.push({[field]: order[orderBy]});
    });
    return query;
}

module.exports = {
    createQuery
}

