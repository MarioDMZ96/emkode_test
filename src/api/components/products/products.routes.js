const express = require('express');
const router = express.Router();
const Products = require('./products.controller');
const { validateProductCreate, validateProductUpdate } = require('../../middlewares/dataValidator');

const productsController = new Products();

router.post( '/products', validateProductCreate, async (req, res) => {
    let result = await productsController.create(req.body);
    if ( result ){
        res.status(200).json({message: 'Product created', product: result});
    } else {
        res.status(400).json({message: 'Product dont created'});
    }
})

router.get( '/products', async (req, res) => {
    let queryParams = req.query;
    let result = await productsController.get(queryParams);
    if ( result.length > 0 ){
        res.status(200).json({message: 'Products found', products: result});
    } else {
        res.status(404).json({message: 'Products not found'});
    }
});

router.get( '/products/:id', async (req, res) => {
    let result = await productsController.getOne(req.params.id);
    if ( result ){
        res.status(200).json({message: 'Product found', product: result});
    } else {
        res.status(404).json({message: 'Product not found'});
    }
})

router.put( '/products/:id', validateProductUpdate,async (req, res) => {
    let id = req.params.id;
    let product = req.body;
    let result = await productsController.updateOne(id, product);
    if ( result ){
        res.status(200).json({message: 'Product updated', product: result});
    } else {
        res.status(404).json({message: 'Product not updated'});
    }
})

router.delete( '/products/:id', async (req, res) => {
    let result = await productsController.deleteOne(req.params.id);
    if ( result ){
        res.status(200).json({message: 'Product deleted', product: result});
    } else {
        res.status(404).json({message: 'Product not deleted'});
    }
})

module.exports = router;