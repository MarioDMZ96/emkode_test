const express = require('express');
const cors = require('cors');
require('dotenv').config();
const routes = require('./routes');

class Server {

    app;
    APP_PORT;

    constructor (){
        this.app = express();
        this.configuration();
        this.middlewares();
        this.routes();
    }

    configuration (){
        this.APP_PORT = process.env.APP_PORT;
    }

    middlewares (){
        this.app.use( cors() );
        this.app.use( express.json() );
    }

    routes (){
        this.app.use( '/api', routes );
    }

    async listen (){
        await this.app.listen(this.APP_PORT);
        console.log('SERVER UP!');
    }
}

module.exports = Server;