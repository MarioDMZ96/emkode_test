const Server = require('./api/server');

const main = async () => {
    const app = new Server();
    await app.listen();
}

main();