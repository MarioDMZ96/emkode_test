module.exports = products = [
    { "id": 1, "name": "Samsung Galaxy S21+", "brand": "Samsung", "createdAt": new Date(), "updatedAt": new Date() },
    { "id": 2, "name": "iPhone 13", "brand": "Apple", "createdAt": new Date(), "updatedAt": new Date() },
    { "id": 3, "name": "Xiaomi Redmi Note 10 Pro", "brand": "Xiaomi", "createdAt": new Date(), "updatedAt": new Date() },
    { "id": 4, "name": "Moto E7", "brand": "Motorola", "createdAt": new Date(), "updatedAt": new Date() },
    { "id": 5, "name": "Huawei Nova 8", "brand": "Huawei", "createdAt": new Date(), "updatedAt": new Date() }
];