const products = require('./products');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const seeding = async () => {
    for (product of products ){
        await prisma.products.create({
            data: product
        });
    }
}

seeding()
    .catch( e => {
        console.log(e);
        process.exit();
    })
    .finally( () => {
        prisma.$disconnect();
    });