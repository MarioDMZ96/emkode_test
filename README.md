# emkode test

API REST for products CRUD

## To exectute the project

1. Install the project dependencies with the followin command:
```
npm install
```

2. Run the migration to create database and table 
```
npx prisma migrate dev --name init
```

3. Run the seeder to pupulating the products table
```
 npx prisma db seed --preview-feature   
```

4. Run the following command to execute the project
```
npm run start
```

5. Run the following command to run the test
```
npm run test
```

## To test each api endpoint
Use the request.http file as alternative to postman, install the "request client" vscode extension.